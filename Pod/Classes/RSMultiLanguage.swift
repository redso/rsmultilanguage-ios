import Foundation

typealias StringsDict = [String:String]

@objc public class RSMultiLanguage : NSObject {
    
    let levels = ["base", "sync", "project"]
    
    var languageLocaleList: [String]    
    var languageDict: [String:StringsDict] = [String:StringsDict]()
    var bundle: NSBundle
    
    public var locale: String
    
    convenience public init(locales : String...) {
        self.init(bundle: NSBundle.mainBundle(), locale: NSLocale.currentLocale(), locales: locales)
    }
    
    convenience public init(bundle: NSBundle, locales : String...) {
        self.init(bundle: bundle, locale: NSLocale.currentLocale(), locales: locales)
    }
    
    convenience public init(locale: NSLocale, locales : [String]) {
        self.init(bundle: NSBundle.mainBundle(), locale: locale, locales: locales)
    }
    
    convenience public init(bundle: NSBundle, locale: NSLocale, locales : String...) {
        self.init(bundle: bundle, locale: locale, locales: locales)
    }
    
    public init(bundle: NSBundle, locale: NSLocale, locales : [String]) {
        self.bundle = bundle
        self.languageLocaleList = locales
        self.locale = self.languageLocaleList[0]
        
        let language = locale.localeIdentifier
        print(language)
        if languageLocaleList.contains(language) {
            self.locale = language
        } 
        super.init()
        buildLanguageData()
    }

    public func getString(key: String) -> String {
        let wordings = languageDict[self.locale]
        if let w = wordings {
            if w.keys.contains(key).boolValue {
                return w[key]!
            }
        }
        NSException(name: "KeyNotFoundException",
            reason: "Key - \(key) not found in language [zh-Hant]",
            userInfo: nil).raise()
        return ""
    }

    func buildLanguageData(){
        languageDict.removeAll()
        
        for locale in languageLocaleList {
            var stringsDict : StringsDict = StringsDict()
            languageDict[locale] = stringsDict

            for level in levels {
                let fileName = locale + "." + level
                let filePath = self.bundle.pathForResource(fileName, ofType: "strings")
                if filePath == nil {
                    printLog(fileName + " is not available")
                    continue
                }
                
                let rawStringDict = NSDictionary(contentsOfFile: filePath!)
                if let dict = rawStringDict {
                    for (rawStringKey, rawStringValue) in dict {
                        stringsDict[rawStringKey as! String] = rawStringValue as? String
                    }
                } else {
                    printLog(fileName + " is not available")
                    continue
                }
            }
            languageDict[locale] = stringsDict
        }
    }

    func printLog(log : String){
         print("[RSMultiLanguage] " + log)
    }

}