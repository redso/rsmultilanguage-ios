import UIKit
import RSMultiLanguage

class ViewController: UIViewController {
    
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let a : RSMultiLanguage = RSMultiLanguage(locales: "zh-Hant", "zh-Hans")
        label.text = a.getString("TXT_APP_Hello")
    }
}

