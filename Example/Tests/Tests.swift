// https://github.com/Quick/Quick

import Quick
import Nimble
import RSMultiLanguage

class TableOfContentsSpec: QuickSpec {
    override func spec() {
        describe("RSMultiLanguage") {
            let bundle = NSBundle(forClass: TableOfContentsSpec.self)
            var language: RSMultiLanguage!
            
            beforeEach {
                language = RSMultiLanguage(bundle: bundle,
                    locale: NSLocale(localeIdentifier: "zh-Hant"),
                    locales: "zh-Hant", "zh-Hans")
            }
            
            it("can translate 'username'") {
                let translated = language.getString("username")
                expect(translated) == "用戶名稱"
            }
            
            it("can translate 'password'") {
                let translated = language.getString("password")
                expect(translated) == "密碼"
            }
            
            it("throws exception if the key is not exists") {
                expect(language.getString("no_such_key")).to(raiseException())
            }
            
            it("can override base with sync file") {
                let translated = language.getString("username_overrided_by_sync")
                expect(translated) == "用戶名稱(sync)"
            }
            
            it("can override sync with project file") {
                let translated = language.getString("username_overrided_by_project")
                expect(translated) == "用戶名稱(project)"
            }
            
            it("can change locale in runtime") {
                language.locale = "zh-Hans"
                let translated = language.getString("username")
                expect(translated) == "用户名称"
            }
            
            it("will use system default locale if in the list") {
                language = RSMultiLanguage(bundle: bundle,
                    locale: NSLocale(localeIdentifier: "zh-Hans"),
                    locales: "zh-Hant", "zh-Hans")
                expect(language.locale) == "zh-Hans"
            }
            
            it("will use first in the list if system locale is not available") {
                language = RSMultiLanguage(bundle: bundle,
                    locale: NSLocale(localeIdentifier: "en"),
                    locales: "zh-Hant", "zh-Hans")
                expect(language.locale) == "zh-Hant"
            }
        }
    }
}
