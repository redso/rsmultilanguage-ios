# RSMultiLanguage

[![Version](https://img.shields.io/cocoapods/v/RSMultiLanguage.svg?style=flat)](http://cocoapods.org/pods/RSMultiLanguage)
[![License](https://img.shields.io/cocoapods/l/RSMultiLanguage.svg?style=flat)](http://cocoapods.org/pods/RSMultiLanguage)
[![Platform](https://img.shields.io/cocoapods/p/RSMultiLanguage.svg?style=flat)](http://cocoapods.org/pods/RSMultiLanguage)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RSMultiLanguage is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "RSMultiLanguage"
```

## Setup

Language files must be added to the project with the name convention: {locale}.base.strings, {locale}.sync.strings, {locale}.project.strings

The translations in project.strings will replace transaltions in sync.strings if keys are conflicted. So are base.strings.

The order is: project > sync > base

Initialize the multiLanguage engine with two parameters:

* locale: the default locale used for the engine
* locales: a list of available locales the app supports

Note: This initialize should be done once only, it is suggest to use AppDelegate to hold one instance of the engine within the app

```
self.multiLanguage = [[RSMultiLanguage alloc] initWithLocale:[NSLocale localeWithLocaleIdentifier:@"en"]
                                                     locales:@[@"en", @"zh-Hant", @"zh-Hans"]];
```

After that, translation can be obtained via:

```
[self.multiLanguage getString:key];
```

## Author

Roy Ng, roytornado@gmail.com

## License

RSMultiLanguage is available under the MIT license. See the LICENSE file for more info.
